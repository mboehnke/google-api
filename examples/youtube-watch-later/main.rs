use google_api::YouTubeApi;

const SECRET_FILE: &str = "credentials/google-credentials.json";
const TOKEN_FILE: &str = "credentials/youtube-token.json";
const WATCH_LATER_PLAYLIST: &str = "watch later";

type Error = Box<dyn std::error::Error>;

#[tokio::main]
async fn main() -> Result<(), Error> {
    let api = YouTubeApi::new(SECRET_FILE, TOKEN_FILE).await?;

    let playlist_id = api.playlist_id(WATCH_LATER_PLAYLIST).await?;
    println!("{:?} ({:?})", WATCH_LATER_PLAYLIST, playlist_id);

    let videos = api.playlist_items(&playlist_id).await?;
    println!("videos:\n{:#?}\n{} total", videos, videos.len());

    for (id, video) in videos {
        println!("deleting video from playlist: {:?}", video.title);
        api.delete_playlist_item(&id).await?;
    }

    let playlist_id = api.playlist_id(WATCH_LATER_PLAYLIST).await?;
    println!("{:?} ({:?})", WATCH_LATER_PLAYLIST, playlist_id);

    let videos = api.playlist_items(&playlist_id).await?;
    println!("videos:\n{:#?}\n{} total", videos, videos.len());

    Ok(())
}
