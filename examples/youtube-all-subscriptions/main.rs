use chrono::Duration;
use google_api::YouTubeApi;

const SECRET_FILE: &str = "credentials/google-credentials.json";
const TOKEN_FILE: &str = "credentials/youtube-token.json";

type Error = Box<dyn std::error::Error>;

#[tokio::main]
async fn main() -> Result<(), Error> {
    let api = YouTubeApi::new(SECRET_FILE, TOKEN_FILE).await?;

    let channels = api.subscribed_channels().await?;

    println!("subscriptions:\n{:?}\n{} total", channels, channels.len());

    println!("uploads:");

    let mut total = 0;
    let mut errors = 0;

    for channel_id in channels {
        match api.channel_uploads(&channel_id, Duration::days(14)).await {
            Ok(uploads) => {
                for upload in uploads {
                    print!("{} ", upload.id);
                    total += 1;
                }
            }
            Err(_) => errors += 1,
        }
    }

    println!("\n{} total ({} errors)", total, errors);

    Ok(())
}
