use chrono::Duration;
use google_api::YouTubeApi;

const SECRET_FILE: &str = "credentials/google-credentials.json";
const TOKEN_FILE: &str = "credentials/youtube-token.json";

type Error = Box<dyn std::error::Error>;

#[tokio::main]
async fn main() -> Result<(), Error> {
    let api = YouTubeApi::new(SECRET_FILE, TOKEN_FILE).await?;

    let channels = api.subscribed_channels().await?;

    println!("subscriptions:\n{:?}\n{} total", channels, channels.len());

    for channel_id in channels {
        let uploads = api.channel_uploads(&channel_id, Duration::days(14)).await?;
        if uploads.len() >= 2 {
            println!(
                "2 latest uploads for channel {:?} ({}):\n{:#?}",
                uploads[0].channel,
                channel_id,
                &uploads[0..2]
            );
            break;
        }
    }

    Ok(())
}
