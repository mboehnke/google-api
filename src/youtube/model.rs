use std::convert::TryFrom;

use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

use crate::GoogleApiError;

#[derive(Debug, Clone)]
pub struct VideoInfo {
    pub upload_date: DateTime<Utc>,
    pub title: String,
    pub description: String,
    pub channel: String,
    pub id: String,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct SubscriptionQuery {
    pub part: String,
    pub mine: bool,
    pub max_results: u32,
    pub page_token: String,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ChannelInfoQuery {
    pub part: String,
    pub id: String,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct PlaylistQuery {
    pub part: String,
    pub playlist_id: String,
    pub max_results: u32,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct SubscriptionListResponse {
    pub items: Vec<SubscriptionItem>,
    #[serde(default)]
    pub next_page_token: String,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct SubscriptionItem {
    pub snippet: ChannelSnippet,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ChannelSnippet {
    pub resource_id: ChannelResourceId,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ChannelResourceId {
    pub channel_id: String,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ChannelListResponse {
    pub items: Vec<ChannelItem>,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ChannelItem {
    pub content_details: ContentDetails,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ContentDetails {
    pub related_playlists: RelatedPlaylists,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct RelatedPlaylists {
    pub uploads: String,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct PlaylistItemListResponse {
    pub items: Vec<PlaylistItem>,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct PlaylistItem {
    pub id: String,
    pub snippet: VideoSnippet,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct VideoSnippet {
    pub published_at: String,
    pub title: String,
    pub description: String,
    pub video_owner_channel_title: String,
    pub resource_id: ResourceId,
    pub channel_id: String,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ResourceId {
    pub video_id: String,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct PlaylistListResponse {
    pub items: Vec<Playlist>,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Playlist {
    pub id: String,
    pub snippet: PlaylistSnippet,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct PlaylistSnippet {
    pub title: String,
}

impl TryFrom<VideoSnippet> for VideoInfo {
    type Error = GoogleApiError;

    fn try_from(snippet: VideoSnippet) -> Result<Self, Self::Error> {
        let upload_date = DateTime::parse_from_rfc3339(&snippet.published_at)
            .map_err(|source| GoogleApiError::DatetimeParseError {
                source,
                date: snippet.published_at.clone(),
            })?
            .into();
        Ok(VideoInfo {
            channel: snippet.video_owner_channel_title,
            title: snippet.title,
            id: snippet.resource_id.video_id,
            description: snippet.description,
            upload_date,
        })
    }
}
