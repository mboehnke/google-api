mod model;

use std::convert::TryFrom;

use crate::{Api, GoogleApiError, IntoString};
pub use model::VideoInfo;
use model::*;

use apply::Apply;
use camino::Utf8Path;
use chrono::{Duration, Utc};
use pretend::{pretend, request, resolver::UrlResolver, Json, Pretend};
use pretend_reqwest::Client;

pub struct YouTubeApi(Pretend<Client, UrlResolver>);

const BASE_URL: &str = "https://www.googleapis.com/youtube/v3/";
const SCOPES: [&str; 1] = ["https://www.googleapis.com/auth/youtube"];

impl YouTubeApi {
    pub async fn new(
        secret_file: impl AsRef<Utf8Path>,
        token_file: impl AsRef<Utf8Path>,
    ) -> Result<Self, GoogleApiError> {
        Api::new(BASE_URL, &SCOPES)?
            .authenticate(secret_file, token_file)
            .await
            .map(YouTubeApi)
    }
}

#[pretend]
trait YouTube {
    #[request(method = "GET", path = "subscriptions")]
    async fn subscribed_channels(
        &self,
        query: &SubscriptionQuery,
    ) -> pretend::Result<Json<SubscriptionListResponse>>;

    #[request(method = "GET", path = "channels")]
    async fn channel_info(
        &self,
        query: &ChannelInfoQuery,
    ) -> pretend::Result<Json<ChannelListResponse>>;

    #[request(method = "GET", path = "playlistItems")]
    async fn playlist_items(
        &self,
        query: &PlaylistQuery,
    ) -> pretend::Result<Json<PlaylistItemListResponse>>;

    #[request(
        method = "GET",
        path = "playlists?part=snippet&mine=true&maxResults=50"
    )]
    async fn my_playlists(&self) -> pretend::Result<Json<PlaylistListResponse>>;

    #[request(
        method = "DELETE",
        path = "playlistItems?id={playlist_item_id}&maxResults=50"
    )]
    async fn delete_playlist_item(&self, playlist_item_id: &str) -> pretend::Result<()>;
}

impl YouTubeApi {
    pub async fn subscribed_channels(&self) -> Result<Vec<String>, GoogleApiError> {
        let mut page_token = String::new();
        let mut channels = Vec::new();
        loop {
            let query = SubscriptionQuery {
                part: "snippet".to_string(),
                mine: true,
                max_results: 50,
                page_token,
            };
            let response: SubscriptionListResponse = self
                .0
                .subscribed_channels(&query)
                .await
                .map_err(|source| GoogleApiError::ApiRequestError(source.into_string()))?
                .value();
            let c = response
                .items
                .into_iter()
                .map(|i| i.snippet.resource_id.channel_id);
            channels.extend(c);
            if response.next_page_token.is_empty() {
                break;
            }
            page_token = response.next_page_token;
        }
        Ok(channels)
    }

    /// gets all uploads from all subscribed channels (max 50 per channel)
    ///
    /// tries to get as many videos as possible and will keep going even if some requests fail
    pub async fn subscription_uploads(
        &self,
        max_age: Duration,
    ) -> Result<Vec<VideoInfo>, GoogleApiError> {
        let mut videos = vec![];
        for channel_id in self.subscribed_channels().await? {
            if let Ok(mut v) = self.channel_uploads(&channel_id, max_age).await {
                videos.append(&mut v)
            }
        }
        Ok(videos)
    }

    pub async fn channel_uploads(
        &self,
        channel_id: &str,
        max_age: Duration,
    ) -> Result<Vec<VideoInfo>, GoogleApiError> {
        let playlist_id = self
            .channel_info(channel_id)
            .await?
            .content_details
            .related_playlists
            .uploads;
        let now = Utc::now();
        self.playlist_items(&playlist_id)
            .await?
            .into_iter()
            .map(|(_, video)| video)
            .filter(|v| v.upload_date + max_age >= now)
            .collect::<Vec<_>>()
            .apply(Ok)
    }

    pub async fn playlist_items(
        &self,
        playlist_id: &str,
    ) -> Result<Vec<(String, VideoInfo)>, GoogleApiError> {
        let query = PlaylistQuery {
            part: "snippet".to_string(),
            playlist_id: playlist_id.to_string(),
            max_results: 50,
        };
        let response: PlaylistItemListResponse = self
            .0
            .playlist_items(&query)
            .await
            .map_err(|source| GoogleApiError::ApiRequestError(source.into_string()))?
            .value();
        response
            .items
            .into_iter()
            .map(|i| {
                let id = i.id;
                let video = VideoInfo::try_from(i.snippet);
                video.map(|video| (id, video))
            })
            .collect()
    }

    pub async fn playlist_id(&self, playlist_name: &str) -> Result<String, GoogleApiError> {
        let response: PlaylistListResponse = self
            .0
            .my_playlists()
            .await
            .map_err(|source| GoogleApiError::ApiRequestError(source.into_string()))?
            .value();
        response
            .items
            .into_iter()
            .find(|p| p.snippet.title == playlist_name)
            .ok_or_else(|| GoogleApiError::PlaylistNotFoundError(playlist_name.to_string()))
            .map(|p| p.id)
    }

    pub async fn delete_playlist_item(&self, playlist_item_id: &str) -> Result<(), GoogleApiError> {
        self.0
            .delete_playlist_item(playlist_item_id)
            .await
            .map_err(|source| GoogleApiError::ApiRequestError(source.into_string()))
    }

    async fn channel_info(&self, channel_id: &str) -> Result<ChannelItem, GoogleApiError> {
        let query = ChannelInfoQuery {
            part: "contentDetails".to_string(),
            id: channel_id.to_string(),
        };
        let response: ChannelListResponse = self
            .0
            .channel_info(&query)
            .await
            .map_err(|source| GoogleApiError::ApiRequestError(source.into_string()))?
            .value();

        response
            .items
            .first()
            .cloned()
            .ok_or_else(|| GoogleApiError::ChannelNotFoundError(channel_id.to_string()))
    }
}
