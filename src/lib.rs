mod drive;
mod youtube;

use std::iter::once;

pub use drive::DriveApi;
pub use youtube::{VideoInfo, YouTubeApi};

use apply::Apply;
use camino::{Utf8Path, Utf8PathBuf};
use pretend::{http::header::AUTHORIZATION, resolver::UrlResolver, Pretend, Url};
use pretend_reqwest::{reqwest::ClientBuilder, Client};
use thiserror::Error;
use yup_oauth2::{AccessToken, DeviceFlowAuthenticator};

/// GoogleAPIError enumerates all possible errors returned by this library.
#[derive(Error, Debug)]
#[non_exhaustive]
pub enum GoogleApiError {
    #[error("invalid filename")]
    InvalidFilenameError,

    #[error("failed to parse API response")]
    ApiResponseParseError,

    #[error("could not read file: {file:?}")]
    ReadFileError {
        source: std::io::Error,
        file: Utf8PathBuf,
    },

    #[error("error parsing date: {date:?}")]
    DatetimeParseError {
        source: chrono::ParseError,
        date: String,
    },

    #[error("cannot parse an absolute URL from {0:?}")]
    UrlParseError(String),

    #[error("could not find channel: {0:?}")]
    ChannelNotFoundError(String),

    #[error("could not find playlist: {0:?}")]
    PlaylistNotFoundError(String),

    #[error("cannot parse header value: {val:?}")]
    HeaderValueParseError {
        source: pretend::http::header::InvalidHeaderValue,
        val: String,
    },

    #[error("cannot construct HTTP client")]
    ClientBuildError {
        source: pretend_reqwest::reqwest::Error,
    },

    #[error("API request failed: {0}")]
    ApiRequestError(String),

    #[error("could not get API Key from file: {file:?}")]
    ApplicationSecretError {
        source: std::io::Error,
        file: Utf8PathBuf,
    },

    #[error("could not get Authenticator for token file: {file:?}")]
    AuthenticatorError {
        source: std::io::Error,
        file: Utf8PathBuf,
    },

    #[error("could not get Access Token")]
    TokenError { source: yup_oauth2::Error },
}

struct Api {
    base_url: Url,
    scopes: &'static [&'static str],
}

impl Api {
    pub fn new(base_url: &str, scopes: &'static [&'static str]) -> Result<Self, GoogleApiError> {
        let base_url = base_url
            .parse()
            .map_err(|_| GoogleApiError::UrlParseError(base_url.to_string()))?;
        Ok(Api { base_url, scopes })
    }

    pub async fn authenticate(
        self,
        secret_file: impl AsRef<Utf8Path>,
        token_file: impl AsRef<Utf8Path>,
    ) -> Result<Pretend<Client, UrlResolver>, GoogleApiError> {
        let token = self.get_auth_token(secret_file, token_file).await?;

        let value = format!("Bearer {}", token.as_str())
            .parse()
            .map_err(|source| GoogleApiError::HeaderValueParseError {
                source,
                val: token.as_str().to_string(),
            })?;

        let headers = once((AUTHORIZATION, value)).collect();

        ClientBuilder::new()
            .default_headers(headers)
            .build()
            .map_err(|source| GoogleApiError::ClientBuildError { source })?
            .apply(Client::new)
            .apply(Pretend::for_client)
            .with_url(self.base_url)
            .apply(Ok)
    }

    /// gets an auth token
    async fn get_auth_token(
        &self,
        secret_file: impl AsRef<Utf8Path>,
        token_file: impl AsRef<Utf8Path>,
    ) -> Result<AccessToken, GoogleApiError> {
        // Read application secret from a file.
        // The client secret file contains JSON like `{"installed":{"client_id": ... }}`
        let secret = yup_oauth2::read_application_secret(secret_file.as_ref())
            .await
            .map_err(|source| GoogleApiError::ApplicationSecretError {
                source,
                file: secret_file.as_ref().to_path_buf(),
            })?;

        // Create an authenticator that uses an InstalledFlow to authenticate.
        // The authentication tokens are persisted to the token file.
        // The authenticator takes care of caching tokens to disk and refreshing tokens once they've expired.
        let authenticator = DeviceFlowAuthenticator::builder(secret)
            .persist_tokens_to_disk(token_file.as_ref())
            .build()
            .await
            .map_err(|source| GoogleApiError::AuthenticatorError {
                source,
                file: token_file.as_ref().to_path_buf(),
            })?;

        // does everything to obtain a token that can be sent as Bearer token.
        authenticator
            .token(self.scopes)
            .await
            .map_err(|source| GoogleApiError::TokenError { source })
    }
}

trait IntoString {
    fn into_string(self) -> String;
}

impl IntoString for pretend::Error {
    fn into_string(self) -> String {
        match self {
            pretend::Error::Client(_) => "Failed to create client".to_string(),
            pretend::Error::Request(_) => "Invalid request".to_string(),
            pretend::Error::Response(_) => "Failed to execute request".to_string(),
            pretend::Error::Body(_) => "Failed to read response body".to_string(),
            pretend::Error::Status(s) => format!("HTTP {}", s),
        }
    }
}
