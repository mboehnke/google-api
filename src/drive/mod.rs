mod model;

use crate::{Api, GoogleApiError, IntoString};
use model::*;

use camino::Utf8Path;
use pretend::{pretend, request, resolver::UrlResolver, Pretend, Response};
use pretend_reqwest::Client;

const BASE_URL: &str = "https://www.googleapis.com/upload/drive/v3/";
const SCOPES: [&str; 1] = ["https://www.googleapis.com/auth/drive.file"];

pub struct DriveApi(Pretend<Client, UrlResolver>);

impl DriveApi {
    pub async fn new(
        secret_file: impl AsRef<Utf8Path>,
        token_file: impl AsRef<Utf8Path>,
    ) -> Result<Self, GoogleApiError> {
        Api::new(BASE_URL, &SCOPES)?
            .authenticate(secret_file, token_file)
            .await
            .map(DriveApi)
    }
}

#[pretend]
trait Drive {
    #[request(method = "POST", path = "files?uploadType=resumable")]
    async fn upload_info(&self, json: FileInfo) -> pretend::Result<Response<String>>;

    #[request(method = "PUT", path = "{path}")]
    async fn upload_content(&self, path: &str, body: Vec<u8>) -> pretend::Result<()>;
}

impl DriveApi {
    pub async fn upload(&self, file: &Utf8Path) -> Result<(), GoogleApiError> {
        // upload metadata
        let name = file
            .file_name()
            .ok_or(GoogleApiError::InvalidFilenameError)?
            .to_string();
        let response: Response<String> = self
            .0
            .upload_info(FileInfo { name })
            .await
            .map_err(|source| GoogleApiError::ApiRequestError(source.into_string()))?;

        // did everything go ok?
        if !response.status().is_success() {
            let msg = response.into_body();
            return Err(GoogleApiError::ApiRequestError(msg));
        }

        // get url for actual upload
        let url = response
            .headers()
            .get("Location")
            .ok_or(GoogleApiError::ApiResponseParseError)?
            .to_str()
            .map_err(|_| GoogleApiError::ApiResponseParseError)?;

        // upload data
        let data = tokio::fs::read(file)
            .await
            .map_err(|source| GoogleApiError::ReadFileError {
                source,
                file: file.to_path_buf(),
            })?;
        self.0
            .upload_content(url, data)
            .await
            .map_err(|source| GoogleApiError::ApiRequestError(source.into_string()))
    }
}
